/*You have an array of numbers.
Your task is to sort ascending odd numbers but even numbers must be on their places.*/
/*----------------------------------------*/
/*----------------------------------------*/
import java.util.ArrayList;
import java.util.Collections;
public class Educ {
    public static void main(String[] args) {
        int[] num = {2, 5, 6, 1, 7, 6};
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < num.length; i++) {
            if (num[i] % 2 !=0 ){
                arrayList.add(num[i]);
            }
        }
        Collections.sort(arrayList);
        int j= 0;
        for (int i =0; i< num.length; i++){
            if(num[i]%2!=0){
                num[i] = (int) arrayList.get(j);
                j++;
            }
        }
       System.out.print(arrayList);
        System.out.println();
        for (int i =0; i< num.length; i++){
            System.out.print(num[i]+" ");
        }
    }
}
